# Tugas Besar OOP

## Aplikasi
[Agoda]

## No.1

terdapat beberapa bagian yang merupakan implementasi dari algoritma pemrograman yang mencakup logika pengolahan data dan penggunaan struktur data. Berikut ini adalah bagian-bagian yang termasuk dalam implementasi algoritma pemrograman:

**Pencarian Data (Searching Data):**
   - Metode `getUserByName(name)` digunakan untuk mencari objek `User` berdasarkan nama pengguna (name).
   - Metode `findUserById(userId)` digunakan untuk mencari objek `User` berdasarkan ID pengguna (userId).
   - Metode `getAccommodationName(accommodationId)` digunakan untuk mencari nama akomodasi (accommodation) berdasarkan ID akomodasi (accommodationId).
   - Di dalam metode `viewReservations()`, dilakukan pencarian reservasi berdasarkan nama pengguna yang diberikan.

**Struktur Data List (Daftar):**
   - Daftar `users`, `locations`, `accommodations`, `rooms`, `reservations`, `payments`, dan `promotions` digunakan untuk menyimpan data objek dari kelas-kelas yang relevan (seperti `User`, `Location`, `Accommodation`, dll).

**Pemilihan (Selection) dan Penggunaan Switch-Case:**
   - Di dalam metode `searchAccommodations()` dan `makeReservation()`, digunakan struktur switch-case untuk memilih pilihan yang diinginkan pengguna berdasarkan nomor yang diinputkan.

**Pengulangan (Looping):**
   - Di dalam `main()`, terdapat perulangan `while` untuk menampilkan menu dan mengambil pilihan pengguna hingga pengguna memilih untuk keluar (exit).

**Pengolahan Data:**
   - Di dalam metode `makeReservation()`, terdapat logika untuk melakukan reservasi berdasarkan pilihan yang diberikan pengguna, seperti memilih kota, hotel, tipe kamar, dan tanggal reservasi.

**Inisialisasi Data:**
   - Di dalam metode `initializeData()`, dilakukan inisialisasi data contoh untuk `users`, `locations`, `accommodations`, `rooms`, `reservations`, `payments`, dan `promotions`.



## No.2
**Link Program :**
[AgodaReservationSystem.java](AgodaReservationSystem.java)

## No.3
Konsep dasar OOP (Object-Oriented Programming) adalah paradigma pemrograman yang berfokus pada representasi dunia nyata dalam bentuk objek dan interaksi antara objek-objek tersebut. OOP digunakan untuk mengorganisasi dan memanipulasi kode program agar lebih mudah dimengerti, dipelihara, dan dikembangkan. Terdapat empat pilar utama dalam konsep dasar OOP:

**Enkapsulasi (Encapsulation):**
Enkapsulasi adalah konsep menyembunyikan implementasi internal dari sebuah objek dan hanya mengekspos fungsionalitas yang relevan melalui metode publik. Ini berarti objek harus memiliki atribut (data) dan metode (fungsi) yang saling terkait. Dengan enkapsulasi, kita dapat mencegah akses langsung ke data objek dari luar dan mengontrol bagaimana data tersebut dapat diubah.

**Pewarisan (Inheritance):**
Pewarisan memungkinkan pembuatan kelas baru (kelas turunan atau subclass) yang dapat mewarisi atribut dan metode dari kelas yang sudah ada (kelas induk atau superclass). Dengan pewarisan, kita dapat membuat hierarki kelas dan menghindari duplikasi kode, sehingga memudahkan pengelolaan dan perubahan kode.

**Polimorfisme (Polymorphism):**
Polimorfisme memungkinkan objek dari kelas yang berbeda untuk merespons metode dengan cara yang sama, meskipun mungkin memiliki implementasi yang berbeda. Dengan polimorfisme, kita dapat menggunakan metode yang sama pada objek yang berbeda tanpa harus mengetahui tipe objek sebenarnya.

**Abstraksi (Abstraction):**
Abstraksi adalah konsep menyembunyikan detail implementasi dan hanya mengekspos fitur-fitur yang relevan dari objek. Tujuannya adalah menyederhanakan kompleksitas dengan hanya fokus pada fitur penting dari objek dalam aplikasi yang sedang dibangun.




## No.4

Encapsulation adalah konsep dalam pemrograman berorientasi objek yang menggabungkan data dan fungsi terkait dalam satu unit yang disebut kelas. Dalam kode yang diberikan, terdapat beberapa bagian yang menggambarkan konsep encapsulation.

Kelas-kelas seperti User, Location, Accommodation, Room, Reservation, Payment, dan Promotion merupakan contoh implementasi encapsulation. Setiap kelas ini memiliki variabel-variabel anggota (atribut) yang diakses melalui metode getter dan setter.

Setiap atribut dalam kelas-kelas tersebut dideklarasikan sebagai private, yang berarti mereka tidak dapat diakses secara langsung dari luar kelas. Untuk mengakses atau memodifikasi atribut tersebut, metode getter dan setter harus digunakan.

Berikut adalah bagian kode yang termasuk dalam encapsulation:

```
class User {
  private int userId;
  private String name;
  private String email;
  private String phoneNumber;

  // ...
}

class Location {
  private int locationId;
  private String cityName;

  // ...
}

class Accommodation {
  private int accommodationId;
  private String name;
  private String address;
  private float rating;
  private float pricePerNight;

  // ...
}

class Room {
  private int roomId;
  private String type;
  private float size;

  // ...
}

class Reservation {
  private int reservationId;
  private int userId;
  private int accommodationId;
  private Room room;
  private String startDate;
  private String endDate;
  private float totalPrice;

  // ...
}

class Payment {
  private int paymentId;
  private int reservationId;
  private float amount;
  private String paymentMethod;
  private String status;

  // ...
}

class Promotion {
  private int promotionId;
  private String name;
  private String description;

  // ...
}
```

Pada setiap kelas tersebut, atribut-atributnya dideklarasikan sebagai private, dan metode getter dan setter digunakan untuk mengakses dan memodifikasinya. Contohnya adalah getUserId(), getName(), getEmail() pada kelas User dan getLocationId(), getCityName() pada kelas Location.

Dengan menggunakan encapsulation, data dalam objek-objek tersebut terlindungi dan hanya dapat diakses melalui metode yang ditentukan, sehingga meningkatkan keamanan dan memudahkan pemeliharaan kode.

## No.5

**Kelas User, Location, Accommodation, Room, Reservation, Payment, dan Promotion yang merupakan contoh dari abstraksi dalam bentuk pemodelan objek:**

```
class User {
  // ...

  public User(int userId, String name, String email, String phoneNumber) {
    // ...
  }

  // ...
}

class Location {
  // ...

  public Location(int locationId, String cityName) {
    // ...
  }

  // ...
}

class Accommodation {
  // ...

  public Accommodation(int accommodationId, String name, String address, float rating, float pricePerNight) {
    // ...
  }

  // ...
}

class Room {
  // ...

  public Room(int roomId, String type, float size) {
    // ...
  }

  // ...
}

class Reservation {
  // ...

  public Reservation(int reservationId, int userId, int accommodationId, Room room, String startDate, String endDate) {
    // ...
  }

  // ...
}

class Payment {
  // ...

  public Payment(int paymentId, int reservationId, float amount, String paymentMethod, String status) {
    // ...
  }

  // ...
}

class Promotion {
  // ...

  public Promotion(int promotionId, String name, String description) {
    // ...
  }

  // ...
}
```

**Metode-metode dalam kelas-kelas tersebut yang mengabstraksi detail implementasi:**
```
class AgodaReservationSystem {
  // ...

  private static void searchAccommodations() {
    // ...
  }

  private static void makeReservation() {
    // ...
  }

  private static void viewReservations() {
    // ...
  }

  private static void makePayment() {
    // ...
  }

  private static void initializeData() {
    // ...
  }

  private static User findUserById(int userId) {
    // ...
  }

  private static String getAccommodationName(int accommodationId) {
    // ...
  }
}
```

**Struktur data yang digunakan untuk menyimpan objek-objek:**
```
public class AgodaReservationSystem {
  private static List<User> users = new ArrayList<>();
  private static List<Location> locations = new ArrayList<>();
  private static List<Accommodation> accommodations = new ArrayList<>();
  private static List<Room> rooms = new ArrayList<>();
  private static List<Reservation> reservations = new ArrayList<>();
  private static List<Payment> payments = new ArrayList<>();
  private static List<Promotion> promotions = new ArrayList<>();
  
  // ...
}
```

Dengan menggunakan abstraksi seperti ini, kode program menjadi lebih terstruktur dan mudah diimplementasikan. Abstraksi memungkinkan pemisahan antara bagaimana data disimpan dan diakses, dengan fungsionalitas logika bisnis yang diimplementasikan di dalam metode-metode. Hal ini juga memudahkan untuk memahami dan mengembangkan sistem reservasi yang lebih kompleks dengan menggunakan pendekatan yang lebih terstruktur dan modular.


## No.6

**Inheritance (Pewarisan)**
Contoh Inheritance dapat dilihat pada kelas PremiumAccommodation. Kelas ini merupakan subkelas dari kelas Accommodation. Berikut adalah bagian kode yang menunjukkan inheritance:
```
class PremiumAccommodation extends Accommodation {
    private String additionalServices;

    public PremiumAccommodation(int accommodationId, String name, String address, float rating, float pricePerNight, String additionalServices) {
        super(accommodationId, name, address, rating, pricePerNight);
        this.additionalServices = additionalServices;
    }

    // Getters dan setters untuk additionalServices
}
```

Penjelasan:

Kelas PremiumAccommodation memiliki kata kunci extends Accommodation, yang menandakan bahwa PremiumAccommodation merupakan subkelas dari Accommodation.
Dengan pewarisan ini, kelas PremiumAccommodation akan memiliki semua atribut dan metode yang dimiliki oleh kelas Accommodation. Dengan kata lain, PremiumAccommodation "mewarisi" sifat dan perilaku dari Accommodation.


**Polymorphism (Polimorfisme)**
Berikut adalah bagian-bagian kode yang menunjukkan contoh polimorfisme melalui metode findUserById dan getAccommodationName.

**Polymorphism - Metode findUserById:**
```
private static User findUserById(int userId) {
    for (User user : users) {
        if (user.getUserId() == userId) {
            return user;
        }
    }
    return null;
}
```

**Polymorphism - Metode getAccommodationName:**
```
private static String getAccommodationName(int accommodationId) {
    for (Accommodation accommodation : accommodations) {
        if (accommodation.getAccommodationId() == accommodationId) {
            return accommodation.getName();
        }
    }
    return "Unknown";
}
```

Dalam kedua metode di atas, polimorfisme diperlihatkan dengan cara mereka menerima parameter yang berbeda dan dapat mengembalikan hasil berdasarkan parameter yang diberikan. Metode findUserById menerima userId dan mengembalikan objek dari kelas User, sedangkan metode getAccommodationName menerima accommodationId dan mengembalikan nama akomodasi berdasarkan accommodationId. Kedua metode ini menunjukkan polimorfisme karena dapat digunakan untuk mencari informasi berdasarkan parameter yang berbeda dan bisa memberikan hasil yang sesuai dengan tipe datanya.


## No.7

Untuk mendeskripsikan proses bisnis Aplikasi Agoda ke dalam skema OOP, langkah-langkahnya adalah sebagai berikut:

**Identifikasi Use Case:**
Identifikasi dan pahami dengan jelas setiap langkah dan proses bisnis yang ingin diimplementasikan dalam aplikasi Agoda. Misalnya, pencarian akomodasi, pemesanan kamar, pembayaran, melihat reservasi, dan lain-lain. Setiap langkah atau fitur yang relevan dengan proses bisnis akan menjadi kandidat untuk menjadi kelas-kelas dalam skema OOP.

**Identifikasi Objek dalam Proses Bisnis:**
Identifikasi semua objek atau entitas yang terlibat dalam proses bisnis Agoda. Objek-objek ini nantinya akan diwakili oleh kelas-kelas dalam skema OOP. Contohnya, objek User, Location, Accommodation, Room, Reservation, Payment, dan Promotion.

**Buat Kelas-Kelas:**
Setelah mengidentifikasi objek-objek, buat kelas-kelas yang merepresentasikan setiap objek tersebut. Setiap kelas akan memiliki atribut-atribut dan metode-metode yang sesuai dengan karakteristik dan tingkah laku objek dalam proses bisnis Agoda.

**Definisikan Hubungan Antara Kelas:**
Tentukan hubungan antara kelas-kelas yang ada. Hubungan-hubungan ini bisa berupa relasi inheritance (pewarisan), komposisi (kesatuan penuh), atau asosiasi (keterkaitan). Misalnya, Reservation memiliki relasi "has-a" dengan Room, sehingga kita menyimpan referensi objek Room di dalam Reservation.

**Implementasikan Metode-Metode:**
Implementasikan metode-metode dalam setiap kelas yang diperlukan untuk menjalankan proses bisnis yang sesuai. Metode-metode ini akan mengimplementasikan langkah-langkah dan logika bisnis dalam OOP. Misalnya, dalam kelas AgodaReservationSystem, Anda dapat mengimplementasikan metode searchAccommodations(), makeReservation(), viewReservations(), dan makePayment().

**Buat Driver Program:**
Buat driver program (program utama) yang menggunakan kelas-kelas dan metode-metode yang telah dibuat untuk menjalankan proses bisnis Aplikasi Agoda secara keseluruhan. Anda dapat membuat main() di dalam kelas AgodaReservationSystem seperti pada contoh sebelumnya.

**Uji Coba dan Debug:**
Uji coba program secara menyeluruh untuk memastikan bahwa proses bisnis berjalan sesuai dengan yang diharapkan. Lakukan debug jika ditemukan kesalahan atau masalah dalam implementasi.


## No.8
## Class Diagram
![img](Class_Diagram.jpg)

## Use case table

| No | Aktivitas | Skala Prioritas | User / Admin |
| -- | ------ | ------ | ------ |
|  1 | Pencarian Akomodasi                                                           | Tinggi 85%                | User          |
|  2 | Reservasi Akomodasi                                                           | Tinggi 90%                | User          |
|  3 | Pembayaran                                                                    | Tinggi 95%                | User          |
|  4 | Kelola Data Pemesanan                                                         | Tinggi 80%                | Admin         |
|  5 | Kelola Data Akomodasi                                                         | Tinggi 80%                | Admin         |
|  6 | Verifikasi Pembayaran                                                         | Sedang 75%                | User          |
|  7 | Kelola Data Pengguna                                                          | Tinggi 80%                | Admin         |
|  8 | Notifikasi Reservasi                                                          | Sedang 70%                | User          |
|  9 | Pencarian Berdasarkan Kategori                                                | Sedang 70%                | User          |
| 10 | Ulasan dan Penilaian Akomodasi                                                | Sedang 75%                | User          |
| 11 | Penanganan Keluhan Pelanggan                                                  | Tinggi 80%                | Admin         |
| 12 | Integrasi Dengan Sistem Pembayaran                                            | Tinggi 85%                | Admin         |
| 13 | Kelola Ketersediaan Akomodasi                                                 | Tinggi 90%                | Admin         |
| 14 | Pengelolaan Promosi                                                           | Tinggi 85%                | Admin         |
| 15 | Pemesanan Layanan Tambahan                                                    | Sedang 70%                | User          |
| 16 | Sistem Poin Atau Reward                                                       | Sedang 70%                | User          |
| 17 | Pencarian Akomodasi Berdasarkan Harga                                         | Tinggi 80%                | User          |
| 18 | Penjadwalan Ulang Pemesanan                                                   | Sedang 75%                | User          |
| 19 | Sistem Notifikasi Harga Terbaik                                               | Sedang 70%                | User          |
| 20 | Integrasi dengan peta dan navigasi                                            | Sedang 70%                | User          |
| 21 | Pencarian Akomodasi Berdasarkan Fasilitas                                     | Sedang 70%                | User          |
| 22 | Kelola Ulasan Pengguna                                                        | Sedang 70%                | Admin         |
| 23 | Penjadwalan Pengingat Reservasi                                               | Rendah 60%                | User          |
| 24 | Pencarian Akomodasi Dengan Aksesibilitas                                      | Sedang 70%                | User          |
| 25 | Integrasi Dengan Sistem Transportasi                                          | Tinggi 80%                | User          |
| 26 | Layanan Pelanggan 24/7                                                        | Tinggi 85%                | User          |
| 27 | Integrasi Dengan Platform Perjalanan Lainnya                                  | Tinggi 85%                | User          |
| 28 | Kelola Kebijakan Pembatalan                                                   | Tinggi 90%                | Admin         |
| 29 | Integrasi Dengan Sistem Loyalty Program                                       | Sedang 75%                | User          |
| 30 | Pencarian Akomodasi Berdasarkan Aktivitas                                     | Sedang 70%                | User          |
| 31 | Pemberitahuan Perubahan Harga                                                 | Sedang 75%                | User          |
| 32 | Kelola Data Mitra Akomodasi                                                   | Sedang 65%                | Admin         |
| 33 | Fitur Wishlist Atau Daftar Favorit                                            | Sedang 70%                | User          |
| 34 | Sistem Rekomendasi Akomodasi                                                  | Sedang 75%                | User          |
| 35 | Kelola Penawaran Spesial Dan Diskon                                           | Tinggi 80%                | Admin         |
| 36 | Fitur Akomodasi Berdasarkan Tanggal                                           | Sedang 75%                | User          |
| 37 | Kelola Data Tagihan Dan Faktur                                                | Tinggi 85%                | Admin         |
| 38 | Fitur Perbandingan Harga Dengan Pesaing                                       | Sedang 75%                | User          |
| 39 | Kelola Program Aflliasi                                                       | Tinggi 80%                | Admin         |
| 40 | Fitur Pencarian Akomodasi Dekat Landmark                                      | Sedang 70%                | User          |
| 41 | Fitur Penyimpanan Preferensi Pengguna                                         | Sedang 70%                | User          |
| 42 | Kelola Laporan Dan Analisis Data                                              | Tinggi 85%                | Admin         |
| 43 | Pemesanan Akomodasi Dengan Voucher                                            | Tinggi 80%                | User          |
| 44 | Kelola Data Lokasi Akomodasi                                                  | Sedang 75%                | Admin         |
| 45 | Fitur Pemesanan Akomodasi Untuk Acara Khusus                                  | Tinggi 85%                | User          |
| 46 | Kelola Program Kemitraan                                                      | Tinggi 80%                | Admin         |
| 47 | Fitur Pemesanan Dengan Pembayaran Cicilan                                     | Tinggi 80%                | User          |
| 48 | Kelola Data Tarif Dan Harga Akomodasi                                         | Tinggi 85%                | Admin         |
| 49 | Fitur Rekomendasi Berdasarkan Riwayat Pemesanan                               | Sedang 75%                | User          |
| 50 | Fitur Notifikasi Perubahan Harga                                              | Sedang 70%                | User          |
| 51 | Kelola Data Ulasan Dan Penilaian Pengguna                                     | Sedang 75%                | Admin         |
| 52 | Kelola Program Reward Dan Loyalitas                                           | Tinggi 80%                | Admin         |
| 53 | Kelola Data Kebijakan Pembayaran                                              | Tinggi 85%                | Admin         |
| 54 | Kelola Data Kebijakan Pembatalan Akomodasi                                    | Tinggi 85%                | Admin         |
| 55 | Kelola Data Promo Dan Diskon                                                  | Tinggi 85%                | Admin         |
| 56 | Fitur Pemesanan Akomodasi Dengan Opsi Makanan                                 | Sedang 75%                | User          |
| 57 | Kelola Data Kebijakan Privasi Dan Keamanan                                    | Tinggi 80%                | Admin         |
| 58 | Kelola Data Kebijakan Pengembalian Dana                                       | Tinggi 80%                | Admin         |
| 59 | Kelola Data Kebijakan Pembayaran Khusus                                       | Tinggi 85%                | Admin         |
| 60 | Kelola Data Kebijakan Pemesanan Khusus                                        | Tinggi 80%                | Admin         |
| 61 | Kelola Data Kebijakan Penghapusan Data Pengguna                               | Tinggi 80%                | Admin         |
| 62 | Kelola Data Kebijakan Keamanan Akun Pengguna                                  | Tinggi 80%                | Admin         |
| 63 | Kelola Data Kebijakan Keamanan Pembayaran                                     | Tinggi 80%                | Admin         |
| 64 | Kelola Data Kebijakan Privasi Pengguna                                        | Tinggi 80%                | Admin         |
| 65 | Fitur Pemesanan Akomodasi Dengan Layanan 24 Jam                               | Sedang 75%                | User          |
| 66 | Kelola Data Kebijakan Pembatalan Dan Pengembalian Dana                        | Tinggi 80%                | Admin         |
| 67 | Kelola Data Kebijakan Penggunaan Poin Reward                                  | Tinggi 80%                | Admin         |
| 68 | Kelola Data Kebijakan Pengguna Kupon Diskon                                   | Tinggi 80%                | Admin         |
| 69 | Pencarian Akomodasi Dekat Pusat Kota                                          | Sedang 75%                | User          |
| 70 | Fitur Pemesanan Akomodasi Dengan Layanan Antar Makanan                        | Sedang 75%                | User          |
| 71 | Pencarian Akomodasi Dengan Akses Ke Tempat Wisata                             | Sedang 70%                | User          |
| 72 | Pencarian Akomodasi Dengan Jarak Terdekat ke Bandara                          | Sedang 75%                | User          |
| 73 | Pencarian Akomodasi Dengan Jarak Terdekat Ke Transportasi Umum                | Sedang 75%                | User          |
| 74 | Pencarian Akomodasi Dengan Jarak Terdekat Ke Landmark                         | Sedang 75%                | User          |
| 75 | Pencarian penerbangan berdasarkan lokasi keberangkatan dan tujuan             | Tinggi 90%                | User          |
| 76 | Pilihan penerbangan dengan filter waktu, harga, maskapai, dll.                | Sedang 80%                | User          |
| 77 | Melihat detail penerbangan termasuk jadwal, durasi, dan fasilitas             | Sedang 75%                | User          |
| 78 | Memilih kursi atau kelas penerbangan                                          | Sedang 75%                | User          |
| 79 | Memasukkan informasi penumpang, seperti nama dan tanggal lahir                | Tinggi 80%                | User          |
| 80 | Melakukan pembayaran untuk pemesanan penerbangan                              | Tinggi 90%                | User          |
| 81 | Menerima e-tiket dan konfirmasi penerbangan                                   | Tinggi 85%                | User          |
| 82 | Mengelola pemesanan penerbangan, seperti pengubahan atau pembatalan           | Tinggi 90%                | User          |
| 83 | Memberikan ulasan dan rating terhadap pengalaman penerbangan                  | Sedang 75%                | User          |
| 84 | Melakukan integrasi dengan sistem pihak ketiga, seperti maskapai              | Tinggi 80%                | Admin         |
| 85 | Mengelola data maskapai, jadwal penerbangan, dan harga                        | Tinggi 85%                | Admin         |
| 86 | Memantau ketersediaan dan harga penerbangan                                   | Tinggi 85%                | Admin         |
| 87 | Memberikan notifikasi perubahan jadwal atau informasi penerbangan             | Tinggi 90%                | User          |
| 88 | Mengelola informasi penumpang, seperti paspor atau visa                       | Sedang 75%                | Admin         |
| 89 | Mengakses informasi tentang bagasi, berat maksimum, dan biaya                 | Sedang 70%                | User          |
| 90 | Melakukan integrasi dengan sistem keamanan bandara                            | Tinggi 80%                | Admin         |
| 91 | Melakukan pembaruan dan peningkatan fitur aplikasi                            | Tinggi 80%                | Admin         |
| 92 | Melakukan integrasi dengan sistem keamanan bandara                            | Tinggi 80%                | Admin         |
| 93 | Pencarian destinasi wisata berdasarkan lokasi dan kategori                    | Tinggi 80%                | User          |
| 94 | Menerima tiket dan konfirmasi reservasi wisata                                | Tinggi 85%                | User          |
| 95 | Melakukan integrasi dengan sistem keamanan bandara                            | Tinggi 80%                | Admin         |
| 96 | Mengakses informasi kontak atau layanan pelanggan                             | Sedang 70%                | Admin         |
| 97 | Melakukan integrasi dengan mitra penyedia paket wisata                        | Tinggi 80%                | Admin         |
| 98 | Memantau ketersediaan dan harga paket wisata                                  | Tinggi 85%                | User          |
| 99 | Fitur pemesanan akomodasi dengan layanan rental mobil                         | Sedang 70%                | User          |
| 100 | Pencarian akomodasi dekat area alun-alun atau taman kota bandara             | Sedang 70%                | User          |

## No.9

## No.10

Berikut adalah beberapa contoh inovasi UX yang diterapkan:

![img](Screenshot__465_.png)

![img](Screenshot__469_.png)

![img](Screenshot__470_.png)

![img](Screenshot__471_.png)

![img](Screenshot__472_.png)


- Tampilan Menu yang Jelas: Pada metode `showMenu()`, terdapat tampilan menu yang jelas dan terstruktur dengan baik. Pengguna dapat melihat pilihan yang tersedia secara langsung dan memilih opsi yang diinginkan dengan mudah.

- Input yang User-Friendly: Pada metode `searchAccommodations()`, `makeReservation()`, dan `makePayment()`, pengguna diberikan petunjuk yang jelas mengenai input yang harus dimasukkan. Contohnya, pengguna diberikan daftar kota yang tersedia dan diminta untuk memasukkan nomor kota yang sesuai. Hal ini membantu pengguna untuk mengerti apa yang diharapkan dari mereka dan meminimalkan kesalahan input.

- Informasi yang Relevan dan Terorganisir: Pada metode `searchAccommodations()`, setelah pengguna memilih kota dan hotel, informasi penting seperti jenis kamar yang tersedia, jumlah kamar yang tersedia, dan harga ditampilkan dengan jelas. Hal ini membantu pengguna untuk memperoleh informasi yang relevan secara cepat dan memudahkan mereka dalam membuat keputusan.

- Pesan Kesalahan yang Jelas: Pada beberapa bagian kode, seperti dalam `searchAccommodations()`, `makeReservation()`, dan `makePayment()`, pesan kesalahan ditampilkan ketika pengguna memasukkan input yang tidak valid. Pesan tersebut memberikan umpan balik yang jelas kepada pengguna mengenai masalah yang terjadi dan membantu mereka memperbaiki kesalahan tersebut.

- Tampilan Reservasi yang Teratur: Pada metode `viewReservations()`, reservasi-reservasi yang dimiliki oleh pengguna ditampilkan dengan tampilan yang teratur dan terstruktur. Informasi penting seperti ID reservasi, nama akomodasi, tipe kamar, tanggal mulai dan selesai, serta harga total ditampilkan secara terpisah untuk setiap reservasi. Hal ini membantu pengguna dalam melihat dan memahami informasi reservasi mereka dengan mudah.


