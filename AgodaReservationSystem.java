import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class User {
  private int userId;
  private String name;
  private String email;
  private String phoneNumber;

  public User(int userId, String name, String email, String phoneNumber) {
    this.userId = userId;
    this.name = name;
    this.email = email;
    this.phoneNumber = phoneNumber;
  }

  public int getUserId() {
    return userId;
  }

  public String getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }
}

class Location {
  private int locationId;
  private String cityName;

  public Location(int locationId, String cityName) {
    this.locationId = locationId;
    this.cityName = cityName;
  }

  public int getLocationId() {
    return locationId;
  }

  public String getCityName() {
    return cityName;
  }
}

class Accommodation {
  private int accommodationId;
  private String name;
  private String address;
  private float rating;
  private float pricePerNight;

  public Accommodation(int accommodationId, String name, String address, float rating, float pricePerNight) {
    this.accommodationId = accommodationId;
    this.name = name;
    this.address = address;
    this.rating = rating;
    this.pricePerNight = pricePerNight;
  }

  public int getAccommodationId() {
    return accommodationId;
  }

  public String getName() {
    return name;
  }

  public String getAddress() {
    return address;
  }

  public float getRating() {
    return rating;
  }

  public float getPricePerNight() {
    return pricePerNight;
  }
}

class Room {
  private int roomId;
  private String type;
  private float size;

  public Room(int roomId, String type, float size) {
    this.roomId = roomId;
    this.type = type;
    this.size = size;
  }

  public int getRoomId() {
    return roomId;
  }

  public String getType() {
    return type;
  }

  public float getSize() {
    return size;
  }
}

class Reservation {
  private int reservationId;
  private int userId;
  private int accommodationId;
  private Room room;
  private String startDate;
  private String endDate;
  private float totalPrice;

  public Reservation(int reservationId, int userId, int accommodationId, Room room, String startDate, String endDate) {
    this.reservationId = reservationId;
    this.userId = userId;
    this.accommodationId = accommodationId;
    this.room = room;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public int getReservationId() {
    return reservationId;
  }

  public int getUserId() {
    return userId;
  }

  public int getAccommodationId() {
    return accommodationId;
  }

  public Room getRoom() {
    return room;
  }

  public String getStartDate() {
    return startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public float getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(float totalPrice) {
    this.totalPrice = totalPrice;
  }
}

class Payment {
  private int paymentId;
  private int reservationId;
  private float amount;
  private String paymentMethod;
  private String status;

  public Payment(int paymentId, int reservationId, float amount, String paymentMethod, String status) {
    this.paymentId = paymentId;
    this.reservationId = reservationId;
    this.amount = amount;
    this.paymentMethod = paymentMethod;
    this.status = status;
  }

  public int getPaymentId() {
    return paymentId;
  }

  public int getReservationId() {
    return reservationId;
  }

  public float getAmount() {
    return amount;
  }

  public String getPaymentMethod() {
    return paymentMethod;
  }

  public String getStatus() {
    return status;
  }
}

class Promotion {
  private int promotionId;
  private String name;
  private String description;

  public Promotion(int promotionId, String name, String description) {
    this.promotionId = promotionId;
    this.name = name;
    this.description = description;
  }

  public int getPromotionId() {
    return promotionId;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }
}

class PremiumAccommodation extends Accommodation {
    private String additionalServices;

    public PremiumAccommodation(int accommodationId, String name, String address, float rating, float pricePerNight, String additionalServices) {
        super(accommodationId, name, address, rating, pricePerNight);
        this.additionalServices = additionalServices;
    }

    public String getAdditionalServices() {
        return additionalServices;
    }

    public void setAdditionalServices(String additionalServices) {
        this.additionalServices = additionalServices;
    }
}


public class AgodaReservationSystem {
  private static List<User> users = new ArrayList<>();
  private static List<Location> locations = new ArrayList<>();
  private static List<Accommodation> accommodations = new ArrayList<>();
  private static List<Room> rooms = new ArrayList<>();
  private static List<Reservation> reservations = new ArrayList<>();
  private static List<Payment> payments = new ArrayList<>();
  private static List<Promotion> promotions = new ArrayList<>();
  private static User getUserByName(String name) {
    for (User user : users) {
        if (user.getName().equals(name)) {
            return user;
        }
    }
    return null;
}


  public static void main(String[] args) {
    initializeData();

    Scanner scanner = new Scanner(System.in);
    boolean exit = false;

    while (!exit) {
      showMenu();
      int choice = scanner.nextInt();

      switch (choice) {
        case 1:
          // Search accommodations
          searchAccommodations();
          break;
        case 2:
          // Make a reservation
          makeReservation();
          break;
        case 3:
          // View reservations
          viewReservations();
          break;
        case 4:
          // Make a payment
          makePayment();
          break;
        case 5:
          // Exit
          exit = true;
          break;
        default:
          System.out.println("Invalid choice. Please try again.");
      }
    }

    scanner.close();
  }

  private static void showMenu() {
    System.out.println("===== Agoda Reservation System =====");
    System.out.println("1. Search Accommodations");
    System.out.println("2. Make a Reservation");
    System.out.println("3. View Reservations");
    System.out.println("4. Make a Payment");
    System.out.println("5. Exit");
    System.out.println("====================================");
    System.out.print("Enter your choice: ");
  }

  private static void searchAccommodations() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("===== Search Accommodations =====");
    System.out.println("Available Cities:");
    System.out.println("1. Jakarta");
    System.out.println("2. Surabaya");
    System.out.println("3. Bekasi");
    System.out.println("4. Bandung");
    System.out.println("5. Medan");
    System.out.println("6. Depok");
    System.out.println("7. Tanggerang");
    System.out.println("8. Palembang");
    System.out.println("9. Semarang");
    System.out.println("10. Makassar");
    System.out.println("11. Batam");
    System.out.println("12. Bandar Lampung");
    System.out.println("13. Bogor");
    System.out.println("14. Pekanbaru");
    System.out.println("15. Padang");
    System.out.println("16. Malang");
    System.out.println("17. Samarinda");
    System.out.println("18. Denpasar");
    System.out.println("19. Jayapura");
    System.out.print("Enter the city number: ");
    int cityNumber = scanner.nextInt();

    String cityName;
    switch (cityNumber) {
      case 1:
        cityName = "Jakarta";
        break;
      case 2:
        cityName = "Surabaya";
        break;
      case 3:
        cityName = "Bekasi";
        break;
      case 4:
        cityName = "Bandung";
        break;
      case 5:
        cityName = "Medan";
        break;
      case 6:
        cityName = "Depok";
        break;
      case 7:
        cityName = "Tanggerang";
        break;
      case 8:
        cityName = "Palembang";
        break;
      case 9:
        cityName = "Semarang";
        break;
      case 10:
        cityName = "Makassar";
        break;
      case 11:
        cityName = "Batam";
        break;
      case 12:
        cityName = "Bandar Lampung";
        break;
      case 13:
        cityName = "Bogor";
        break;
      case 14:
        cityName = "Pekanbaru";
        break;
      case 15:
        cityName = "Padang";
        break;
      case 16:
        cityName = "Malang";
        break;
      case 17:
        cityName = "Samarinda";
        break;
      case 18:
        cityName = "Denpasar";
        break;
      case 19:
        cityName = "Jayapura";
        break;
      default:
        System.out.println("Invalid city number.");
        System.out.println("================================");
        return;
    }
    System.out.println("Searching accommodations in " + cityName + "...");
    // TODO: Implement search accommodations logic for the selected city
    System.out.println("================================");
    
    System.out.println("Selected city: " + cityName);

    System.out.println("Available Hotels in " + cityName + ":");
    System.out.println("1. Raffles");
    System.out.println("2. Santika");
    System.out.println("3. Favehotel");
    System.out.println("4. Reddorz");
    System.out.println("5. OYO");
    // Daftar hotel lainnya

    System.out.print("Enter the hotel number: ");
    int hotelNumber = scanner.nextInt();

    String hotelName;
    switch (hotelNumber) {
      case 1:
        hotelName = "Raffles";
        break;
      case 2:
        hotelName = "Santika";
        break;
      case 3:
        hotelName = "Favehotel";
        break;
      case 4:
        hotelName = "Reddorz";
        break;
      case 5:
        hotelName = "OYO";
        break;
      // Kasus lainnya untuk hotel lainnya
      default:
        System.out.println("Invalid hotel number.");
        System.out.println("================================");
        return;
    }

    System.out.println("Selected hotel: " + hotelName);

    System.out.println("Available Room Types in " + hotelName + ":");
    System.out.println("1. Standard Room");
    System.out.println("2. Superior Room");
    System.out.println("3. Deluxe Room");

    System.out.print("Enter the room type number: ");
    int roomTypeNumber = scanner.nextInt();

    String roomType;
    int availableRooms;
    int price;
    switch (roomTypeNumber) {
      case 1:
        roomType = "Standard Room";
        availableRooms = 14;
        price = 250000;
        break;
      case 2:
                roomType = "Superior Room";
        availableRooms = 10;
        price = 300000;
        break;
      case 3:
        roomType = "Deluxe Room";
        availableRooms = 6;
        price = 500000;
        break;
      default:
        System.out.println("Invalid room type number.");
        System.out.println("================================");
        return;
    }

    System.out.println("Selected room type: " + roomType);
    System.out.println("Available rooms: " + availableRooms);
    System.out.println("Price: " + price);

    

  }

  private static void makeReservation() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("===== Make a Reservation =====");
    System.out.print("Enter your name: ");
    String name = scanner.nextLine();
    System.out.print("Enter your phone number: ");
    String phoneNumber = scanner.nextLine();
    System.out.print("Enter the reservation date (YYYY-MM-DD): ");
    String reservationDate = scanner.nextLine();
    System.out.print("Enter the checkout date (YYYY-MM-DD): ");
    String checkoutDate = scanner.nextLine();

    System.out.println("Available Cities:");
    System.out.println("1. Jakarta");
    System.out.println("2. Surabaya");
    System.out.println("3. Bekasi");
    System.out.println("4. Bandung");
    System.out.println("5. Medan");
    System.out.println("6. Depok");
    System.out.println("7. Tanggerang");
    System.out.println("8. Palembang");
    System.out.println("9. Semarang");
    System.out.println("10. Makassar");
    System.out.println("11. Batam");
    System.out.println("12. Bandar Lampung");
    System.out.println("13. Bogor");
    System.out.println("14. Pekanbaru");
    System.out.println("15. Padang");
    System.out.println("16. Malang");
    System.out.println("17. Samarinda");
    System.out.println("18. Denpasar");
    System.out.println("19. Jayapura");
    // Daftar kota lainnya

    System.out.print("Enter the city number: ");
    int cityNumber = scanner.nextInt();

    String cityName;
    switch (cityNumber) {
      case 1:
        cityName = "Jakarta";
        break;
      case 2:
        cityName = "Surabaya";
        break;
      case 3:
        cityName = "Bekasi";
        break;
      case 4:
        cityName = "Bandung";
        break;
      case 5:
        cityName = "Medan";
        break;
      case 6:
        cityName = "Depok";
        break;
      case 7:
        cityName = "Tanggerang";
        break;
      case 8:
        cityName = "Palembang";
        break;
      case 9:
        cityName = "Semarang";
        break;
      case 10:
        cityName = "Makassar";
        break;
      case 11:
        cityName = "Batam";
        break;
      case 12:
        cityName = "Bandar Lampung";
        break;
      case 13:
        cityName = "Bogor";
        break;
      case 14:
        cityName = "Pekanbaru";
        break;
      case 15:
        cityName = "Padang";
        break;
      case 16:
        cityName = "Malang";
        break;
      case 17:
        cityName = "Samarinda";
        break;
      case 18:
        cityName = "Denpasar";
        break;
      case 19:
        cityName = "Jayapura";
        break;
      // Kasus lainnya untuk kota lainnya
      default:
        System.out.println("Invalid city number.");
        System.out.println("================================");
        return;
    }

    System.out.println("Selected city: " + cityName);

    System.out.println("Available Hotels in " + cityName + ":");
    System.out.println("1. Raffles");
    System.out.println("2. Santika");
    System.out.println("3. Favehotel");
    System.out.println("4. Reddorz");
    System.out.println("5. OYO");
    // Daftar hotel lainnya

    System.out.print("Enter the hotel number: ");
    int hotelNumber = scanner.nextInt();

    String hotelName;
    switch (hotelNumber) {
      case 1:
        hotelName = "Raffles";
        break;
      case 2:
        hotelName = "Santika";
        break;
      case 3:
        hotelName = "Favehotel";
        break;
      case 4:
        hotelName = "Reddorz";
        break;
      case 5:
        hotelName = "OYO";
        break;
      // Kasus lainnya untuk hotel lainnya
      default:
        System.out.println("Invalid hotel number.");
        System.out.println("================================");
        return;
    }

    System.out.println("Selected hotel: " + hotelName);

    System.out.println("Available Room Types in " + hotelName + ":");
    System.out.println("1. Standard Room (250,000 IDR)");
    System.out.println("2. Superior Room (300,000 IDR)");
    System.out.println("3. Deluxe Room (500,000 IDR)");

    System.out.print("Enter the room type number: ");
    int roomTypeNumber = scanner.nextInt();

    String roomType;
    double roomPrice;
    switch (roomTypeNumber) {
      case 1:
        roomType = "Standard Room";
        roomPrice = 250000;
        break;
      case 2:
        roomType = "Superior Room";
        roomPrice = 300000;
        break;
      case 3:
        roomType = "Deluxe Room";
        roomPrice = 500000;
        break;
      default:
        System.out.println("Invalid room type number.");
        System.out.println("================================");
        return;
    }

    System.out.println("Selected room type: " + roomType);
    System.out.println("Room price: " + roomPrice + " IDR");

    // TODO: Implement reservation logic based on the selected options

    System.out.println("================================");
  }

  private static void viewReservations() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("===== View Reservations =====");
    System.out.print("Enter your name: ");
    String name = scanner.nextLine();

    List<Reservation> userReservations = new ArrayList<>();
    for (Reservation reservation : reservations) {
        int userId = reservation.getUserId();
        User user = findUserById(userId);
        if (user != null && user.getName().equals(name)) {
            userReservations.add(reservation);
        }
    }

    if (userReservations.isEmpty()) {
        System.out.println("No reservations found for user: " + name);
    } else {
        System.out.println("Reservations for user: " + name);
        for (Reservation reservation : userReservations) {
            System.out.println("Reservation ID: " + reservation.getReservationId());
            System.out.println("Accommodation: " + getAccommodationName(reservation.getAccommodationId()));
            System.out.println("Room Type: " + reservation.getRoom().getType());
            System.out.println("Start Date: " + reservation.getStartDate());
            System.out.println("End Date: " + reservation.getEndDate());
            System.out.println("Total Price: " + reservation.getTotalPrice());
            System.out.println("==============================");
        }
    }

    System.out.println("================================");
}

  private static void makePayment() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("===== Make a Payment =====");
    System.out.println("Available Payment Methods:");
    System.out.println("1. Bank Transfer");
    System.out.println("2. OVO");
    System.out.println("3. DANA");
    System.out.println("4. LinkAja");
    System.out.println("5. PayPal");
    System.out.println("6. Credit Card");
    System.out.print("Enter the payment method number: ");
    int paymentMethodNumber = scanner.nextInt();

    String paymentMethod;
    switch (paymentMethodNumber) {
      case 1:
        paymentMethod = "Bank Transfer";
        break;
      case 2:
        paymentMethod = "OVO";
        break;
      case 3:
        paymentMethod = "DANA";
        break;
      case 4:
        paymentMethod = "LinkAja";
        break;
      case 5:
        paymentMethod = "PayPal";
        break;
      case 6:
        paymentMethod = "Credit Card";
        break;
      default:
        System.out.println("Invalid payment method number.");
        System.out.println("================================");
        return;
    }

    System.out.println("Selected payment method: " + paymentMethod);

    // TODO: Implement payment logic based on the selected payment method

    System.out.println("================================");
  }

    private static void initializeData() {
    // Initialize sample data
    users.add(new User(1, "John Doe", "john.doe@example.com", "1234567890"));
    users.add(new User(2, "Jane Smith", "jane.smith@example.com", "9876543210"));

    locations.add(new Location(1, "New York"));
    locations.add(new Location(2, "Los Angeles"));

    accommodations.add(new Accommodation(1, "Hotel A", "123 Main St, New York", 4.5f, 100.0f));
    accommodations.add(new Accommodation(2, "Hotel B", "456 Elm St, Los Angeles", 4.0f, 80.0f));

    rooms.add(new Room(1, "Single", 20.0f));
    rooms.add(new Room(2, "Double", 30.0f));

    reservations.add(new Reservation(1, 1, 1, rooms.get(0), "2023-06-01", "2023-06-05"));
    reservations.add(new Reservation(2, 2, 2, rooms.get(1), "2023-07-01", "2023-07-05"));

    payments.add(new Payment(1, 1, 400.0f, "Credit Card", "Paid"));
    payments.add(new Payment(2, 2, 320.0f, "PayPal", "Pending"));

    promotions.add(new Promotion(1, "Summer Sale", "Get 20% off on bookings made in June-August"));
    promotions.add(new Promotion(2, "Weekend Getaway", "Enjoy a discounted rate for weekend stays"));
  }
    private static User findUserById(int userId) {
    for (User user : users) {
        if (user.getUserId() == userId) {
            return user;
        }
    }
    return null;
}

private static String getAccommodationName(int accommodationId) {
    for (Accommodation accommodation : accommodations) {
        if (accommodation.getAccommodationId() == accommodationId) {
            return accommodation.getName();
        }
    }
    return "Unknown";
}

}
